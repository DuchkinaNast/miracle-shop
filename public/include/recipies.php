<?php
session_start();
include("functions.php");
include("auth_cookie.php");
?>

<!DOCTYPE html>

<head lang="en">
  <meta charset="UTF-8">
  <title>Recipe</title>
	  <link href="css/media/media2.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="css/style.css" type="text/css">

		<script type="text/javascript" src="js/jq.js"></script>
  <style>
    body {
      padding: 1%;
    }
    
    .carousel {
      width: 90%;
      padding: 1% 4% 1% 4%;
      border: 1px solid #f7a849;
      border-radius: 15px;
      background: #fbffa6;
      animation-name: display;
      animation-duration: 4s;
    }
    
    #title {
      font-size: 24px;
      font-style: italic;
      color: #f7a849;
    }
    
    hr {
      border: none;
      /* Убираем границу для браузера Firefox */
      color: #f7a849;
      /* Цвет линии для браузера Internet Explorer */
      background-color: #f7a849;
      /* Цвет линии для браузера Firefox и Opera */
      height: 1px;
      /* Толщина линии */
    }
    
    @keyframes display {
      0% {
        opacity: 0;
      }
      99% {
        opacity: 1;
      }
    }
    
    #text {
      font-size: 18px;
      font-style: italic;
    }
    
    .recipe {
      padding: 3%;
      border-radius: 10px;
      border: 1px solid #f7a849;
      font-size: 18px;
      font-style: italic;
    }
    
    .recipe li {
      list-style-type: circle;
      list-style-image: none;
    }
  </style>
</head>

<body>
  <div id="pagewrap">
    <?php include("include/block-header.htm"); ?>
      <div id="content">
        <div id="carousel" class="carousel" onclick="change_text()">
          <span id="title">А знаете ли вы, что...<hr/></span>
          <div id="text" align="justify"></div>
        </div>
        <br/>
        <br/>
        <div id="recipe"></div>
      </div>
      <?php include("include/block-sidebar.htm");?>
  </div>
  <?php include("include/block-left-menu.htm");?>

    <script>
      var text = ["Чтобы собрать нектар для одной ложки меда (30г), пчела  должна сделать 200 вылетов." +
        "Во время интенсивного медосбора она делает за день приблизительно 10 вылетов." +
        "Таким образом, чтобы угостить вас ложечкой душистого меда, 200 пчел должны трудиться полный рабочий день." +
        "Во время медосбора за нектаром летает около половины пчел семьи. Такимобразом, в семье массой 5 кг за нектаром вылетает 2,5 кг или  25 тыс. пчел." +
        "Если каждая пчела сделает 10 вылетов, то все вылетающие пчелы сделают  250 тыс. полетов и могут принести в улей 7,5 кг нектара." +
        "При этом пчелы пролетают расстояние, которое составляет  2 млн. 750 тыс. километров.",
        "Малышам, которым вместо сахара дают мед, лучше набирают вес, не страдают диареей.В их крови увеличивается количество гемоглобина и красных кровяных телец, " +
        "потому что железо, медь и марганец, которые входят в состав меда,способствуют улучшению состава крови.",
        "Чтобы собрать среднюю ношу нектара, пчела должна посетить " +
        "примерно 270 цветков гречки или 140 цветков подсолнечника. В день пчела посещает соответственно 2700 или 1400 цветков, а чтобы собрать нектар для одной ложки меда, пчелы должны " +
        "поработать на 800 тыс. цветков гречки или 400 тыс.цветков подсолнечника.",
        "Мед имеет удивительные свойства: в течении многих лет он сохраняет свои качества, не покрывается плесенью. Это свойство меда использовали еще" +
        "стародавние римляне – они консервировали им редкую дичь, которую доставляли из дальних стран. Залитая медом дичь сохраняла свою свежесть, " +
        "вкус и вид. Известно и другое. Тело Александра Македонского, который умер вовремя похода, было доставлено в столицу Македонии погруженным в мед." +
        "Лишь с открытием антибиотиков, которые были обнаружены и в меде, стали понятны его противомикробные свойства.",
        "Мед помогает выводить алкоголь из организма. Так что во время похмелья неплохо может помочь обычный бутерброд с медом.",
        "Тепло снижает полезные качества меда, поэтому нагревать его не стоит. Температура выше 50 градусов полностью убирает все эти свойства из меда.",
        "Чтобы произвести 100 грамм меда, пчела должна облететь более 100000 цветков.",
        "Мед может не портиться веками, причем сохраняя все свои полезные свойства. При вскрытии могилы Тутанхомона была обнаружена амфора с мёдом. " +
        "Ее вкусовые и полезные качества практически не ухудшились за столько времени."
      ];

      var i = 0;
      document.getElementById('text').innerHTML = text[0];
      function change_text() {
        if (i != text.length - 1) {
          i++;
        } else {
          i = 0;
        }
        document.getElementById('text').innerHTML = text[i];
      }

      $(document).ready(function() {
        $.ajax({
          url: "recipe.php",
          success: function(data) {
            var json = JSON.parse(data);
            var row = "";
            for (var i in json) {
              row +=
                '<div id="recipe_div" class="recipe">' +
                '<div class="name">' + json[i].name + '</div>' +
                '<hr/>' +
                '<div class="ingridients" ><br/>' + json[i].ingridients + '</div>' +
                '<hr/>' +
                '<div align="justify">' + json[i].recipe + '</div><br/></div><br/>';
            }
            $('#recipe').html(row);
          },
          error: function(xhr, str) {
            alert('Ошибка: ' + xhr.responseCode);
          }
        });
      });
    </script>
</body>

</html>