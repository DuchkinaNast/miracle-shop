<?php

session_start();
header('Content-Type: text/html; charset=utf-8');

include 'config.php';
include 'functions.php';

$error = [];

$login = clear_string($connect, $_POST["login"]);
$pass = clear_string($connect, $_POST["pass"]);
$surname = clear_string($connect, $_POST["surname"]);
$name = clear_string($connect, $_POST["name"]);
$patronymic = clear_string($connect, $_POST["patronymic"]);
$email = clear_string($connect, $_POST["email"]);
$phone = clear_string($connect, $_POST["phone"]);
$address = clear_string($connect, $_POST["address"]);

if (strlen($login) < 5 or strlen($login) > 15) {
    $error[] = "Логин должен быть от 5 до 15 символов!";
} else {
    $result = mysqli_query($connect, "SELECT login FROM users WHERE login = '$login'");
    if (mysqli_num_rows($result) > 0) {
        $error[] = "Логин занят!";
    }
}

$result = mysqli_query($connect, "SELECT email FROM users WHERE email = '$email'");

if (mysqli_num_rows($result) > 0) {
    $error[] = "Введенный email уже занят!";
}

if (strlen($pass) < 7 or strlen($pass) > 15) $error[] = 'Укажите пароль от 7 до 15 символов!';
if (strlen($surname) < 3 or strlen($surname) > 15) $error[] = 'Укажите Фамилию от 3 до 15 символов!';
if (strlen($name) < 3 or strlen($name) > 15) $error[] = 'Укажите Имя от 3 до 15 символов!';
if (strlen($patronymic) < 3 or strlen($patronymic) > 15) $error[] = 'Укажите Отчество от 3 до 15 символов!';
if (!preg_match('/^(?:[a-z0-9]+(?:[-_.]?[a-z0-9]+)?@[a-z0-9_.-]+(?:\\.?[a-z0-9]+)?\\.[a-z]{2,5})$/i', trim($email))) $error[] = "Укажите корректный email";
if (!$address) $error[] = 'Укажите номер телефона!';
if (!$phone) $error[] = 'Укажите номер телефона!';

if (count($error)) {
    echo implode('<br>', $error);
    http_response_code(422);
} else {
    $ip = $_SERVER['REMOTE_ADDR'];

    $query = "INSERT INTO users (login, password, name, surname, patronymic, email, phone, address, user_ip, type_of_user) " .
        " VALUES ('$login', '$pass', '$name', '$surname', '$patronymic', '$email', '$phone', '$address', '$ip', 2)";

    $result = mysqli_query($connect, $query);

    if (!$result) {
        echo mysqli_error($connect);
    } else {
        echo 'true';
    }
}
