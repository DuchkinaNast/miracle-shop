<?php

include_once('config.php');//подключаем файл для работы с БД

if (!$connect) {
    die('connection error');
}

if (isset($_GET['sortby'])) {
    $sorting = $_GET['sortby'];
} else {
    $sorting = 'price-desc';
}
switch ($sorting) {
    case 'price-asc';
        $order = 'product.product_price ASC';
        break;

    case 'price-desc';
        $order = 'product.product_price DESC';
        break;

    case 'title-asc';
        $order = 'product.product_name ASC';
        break;

    case 'prod-reiting-desc';
        $order = 'reiting DESC';
        break;

    case 'default';
        $order = 'product.id_product ASC';
        break;

    default:
        $order = 'product.id_product ASC';
        break;
}

$query = "SELECT *, (product.like*100/(product.like + product.deslike)) AS 'reiting' FROM product " .
    "LEFT JOIN category ON product.category_id = category.id_category " .
    "LEFT JOIN liquid ON product.liquid_id = liquid.id_liquid " .
    "LEFT JOIN lightness ON product.lightness_id = lightness.id_lightness " .
    "LEFT JOIN season ON product.season_id = season.id_season " .
    "LEFT JOIN provider ON product.provider_id = provider.id_provider " .
    "LEFT JOIN users ON provider.user_id = users.id_user ORDER BY $order";

$sql = mysqli_query($connect, $query);
$rows = [];//создание массива

while ($result = mysqli_fetch_array($sql)) {
    $rows[] = [
        'name' => $result['product_name'],
        'info' => $result['product_info'],
        'mini_description' => $result['mini_description'],
        'price' => $result['product_price'],
        'like' => $result['like'],
        'dislike' => $result['deslike'],
        'id' => $result['id_product'],
        'url' => $result['url'],
        'properties' => [
            'category' => $result['category'],
            'liquid' => $result['liquid'],
            'lightness' => $result['lightness'],
            'season' => $result['season']
        ],
        'provider' => [
            'provider_surname' => $result['surname'],
            'provider_name' => $result['name'],
            'provider_patronymic' => $result['patronymic'],
            'phone' => $result['phone'],
            'email' => $result['email'],
            'address' => $result['address']
        ],
        'reiting' => $result['reiting']
    ]; //добавление данных в массив rows[]
}

echo json_encode($rows);//возвращает пользователю JSON-представление rows

flush();
