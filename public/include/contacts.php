<?php
session_start();
include('functions.php');
include('auth_cookie.php');
?>
<!DOCTYPE html>
<html>

<head lang="en">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1  user-scalable=no">
  <title>Feedback</title>
  <link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css">
  <link href="css/media/media2.css" rel="stylesheet" type="text/css">
  <link href="css/media/media_contacts.css" rel="stylesheet" type="text/css">
	  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" type="text/css" href="css/contacts.css">
		<script type="text/javascript" src="js/jq.js"></script>
  <script>
    $(document).ready(function() {
      $("#submit_contact_form").click(function() {
        var name = $("#contact_name").val();
        var tel = $("#contact_phone").val();
        var mail = $("#contact_email").val();
        var comment = $("#contact_comments").val();
        datainfo = "data=" + name + "&data1=" + mail + "&data2=" + tel + "&data3=" + comment;

        jQuery.ajax({
          type: "POST",
          url: "comment.php",
          dataType: "text",
          data: datainfo,
          success: function(comment) {

          },
          error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError);
          }
        });
        document.getElementById("contact_form").style.display = "none";
        document.getElementById("forms").style.color = "green";
        document.getElementById("forms").style.fontSize = "22px";
        document.getElementById("forms").innerHTML += "<p style='text-align:left;'>Ваш комментарий отправлен!</p>";
        document.getElementById("forms").innerHTML += "<p style='text-align:left;'><a href='contacts.php'><img src='images/OK.png'/></a></p>";

      });

    });
  </script>
  <style>
    .phone_mail {
      position: absolute;
      margin-top: -10px;
    }
    
    .contact_title {
      padding-top: 10px;
      margin-left: 40px;
      font: bold 16px sans-serif;
    }
    
    #Message {
      font: bold 16px sans-serif;
    }
  </style>
</head>

<body>
  <div id="pagewrap">
    <?php
	include("include/block-header.htm");
?>
      <div id="content">

        <div class="info">
          <img class="phone_mail" src="images/mobile_32.png" /><span class="contact_title">Give us a ring</span>
          <br/>
          <br/>
          <span class="contact_detail">+38 (063) 405 83 84</span>
          <br/>
          <br/>
          <img class="phone_mail" src="images/email_32.png" /><span class="contact_title">Pop us an email</span>
          <br/>
          <br/>
          <span class="contact_detail"><a href="mailto:katevasilchenko2305@gmail.com">katevasilchenko2305@gmail.com</a></span>
          <br/>
          <br/>
          <div id="forms">
            <form id="contact_form">
              <div id="contact_middle_step1" style="height: 300px;">
                <p id="Message">Отправьте нам отзыв:</p>
                <input id="contact_name" class="contact_text" type="text" value="Your name" onFocus="if(this.value=='Your name') this.value='';" onBlur="if(this.value=='') this.value='Your name';" />
                <br/>
                <input id="contact_email" class="contact_text" type="text" value="Your email address" onFocus="if(this.value=='Your email address') this.value='';" onBlur="if(this.value=='') this.value='Your email address';" />
                <br/>
                <input id="contact_phone" class="contact_text" type="text" value="Your phone number" onFocus="if(this.value=='Your phone number') this.value='';" onBlur="if(this.value=='') this.value='Your phone number';" />
                <br/>
                <textarea aria-autocomplete="list" id="contact_comments" class="contact_textarea" onFocus="if(this.value=='How can we help?') this.value='';" onBlur="if(this.value=='') this.value='How can we help?';">How can we help?</textarea>
                <br/>
              </div>
              <input id="submit_contact_form" class="button" type="button" value="Send Message" />
            </form>
          </div>
        </div>
        
        <div id="map">
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1281.7563941368314!2d36.35163789999999!3d50.0204865!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x412709da6678aa77%3A0x7e2a75331dc505ba!2sHeroiv+Pratsi+St%2C+50%2C+Kharkiv%2C+Kharkiv+Oblast!5e0!3m2!1sen!2sua!4v1441918586767" frameborder="0" style="border:1px solid #f7a849" allowfullscreen></iframe>

          <span>OUR ADDRESS:</span>
          <br/>
          <br/>
          <span class="city">Kharkov</span>
          <br/>
          <br/>
          <span class="street">Heroiv Pratsi St.,</span>
          <span class="home">50</span>
          <br/>

        </div>
      </div>
      <?php include("include/block-sidebar.htm");?>
  </div>
  <?php	include("include/block-left-menu.htm");?>

</body>

</html>