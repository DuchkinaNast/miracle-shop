<?php
include('functions.php');
session_start();
include('auth_cookie.php');
//unset($_SESSION['auth']);
setcookie('rememberme','',0,"/");
?>

	<!doctype html>
	<html lang="">

	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title>HoneyShop</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<link rel="stylesheet" href="../css/lib/bootstrap.min.css" />
		<!--		<link rel="stylesheet" href="css/lib/uui-all.css" />-->
		<link rel="stylesheet" href="../css/lib/jquery.mCustomScrollbar.css" />
		<link rel="stylesheet" href="../fonts/font-awesome/css/font-awesome.min.css" />
		<link rel="stylesheet" type="text/css" href="http://fastly.ink.sapo.pt/3.1.10/css/ink.css">
		<link rel="stylesheet" href="../build/css/all.min.css">
		<link rel="stylesheet" href="../css/lib/normalize.css">
	</head>

	<body class="body">
		<header class="header">
			<nav class="navbar navbar-inverse">
				<div class="container">
					<div class="navbar-header">
						<button class="navbar-toggle" data-toggle="collapse" data-target="#nav">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#"><img src="img/logo.svg" alt="HoneyShop" /></a>
					</div>
				</div>
				<div class="collapse navbar-collapse navbar-mystyle" id="nav">
					<div class="container">
						<ul class="nav navbar-nav navbar-right text-uppercase">
							<li>
								<a href="../index.php" id="home" class="nav-item none-underline"><i class="glyphicon glyphicon-home nav-icon"></i>home</a>
							</li>
							<li>
								<a href="#cart" id="cart" class="nav-item none-underline"><i class="glyphicon fa fa-shopping-cart nav-icon"></i>cart</a>
							</li>
							<li>
								<a href="#providers" id="providers" class="nav-item none-underline"><i class="glyphicon glyphicon-user nav-icon"></i>providers</a>
							</li>
							<li>
								<a href="#" id="" class="nav-item none-underline dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="glyphicon fa fa-plus-square nav-icon"></i>registration<span class="caret"></span></a>
								<ul class="dropdown-menu additional-menu">
									<li><a href="#" id="provider" id="providers"><i class="glyphicon glyphicon-briefcase nav-icon"></i>As a provider</a></li>
									<li role="separator" class="divider nav-separator"></li>
									<li><a href="include/registration.php" id="user"><i class="glyphicon glyphicon-user nav-icon"></i>As a user</a></li>
								</ul>
							</li>
							<li>
								<a href="#" id="" class="nav-item none-underline"><i class="glyphicon glyphicon-log-in nav-icon"></i>login</a>
							</li>
						</ul>
					</div>
				</div>
			</nav>
		</header>

		<main class="main container content">
			
            <fieldset>
                <legend>Simply registration form</legend>
            <div class="control-group required">
                <label for="full-name">Full Name</label>
                <div class="control">
                    <input name="text" id="full-name" type="text" placeholder="Please add your name">
                </div>
            </div>
            <div class="control-group required">
                <label for="phone">Telephone</label>
                <div class="control">
                    <input name="text" id="phone" type="text" placeholder="Please add you telephone number">
                </div>
            </div>
            <div class="control-group required">
                <label for="email">Email</label>
                <div class="control">
                    <input name="text" id="email" type="text" placeholder="Please add your email">
                    <i class="fa fa-envelope-o"></i>
                </div>
            </div>
            <div class="control-group required">
                <label for="psw">Password</label>
                <div class="control">
                    <input name="text" id="psw" type="password" placeholder="">
                </div>
                <div>
                    <div class="psw-strength" style="width: 0"></div>
                </div>
                <p class="tip">Use at least one lowercase letter, one numeral, and seven characters.</p>
            </div>
            <div class="control-group required">
                <label for="psw-r">Repeat password</label>
                <div class="control">
                    <input name="text" id="psw-r" type="password" placeholder="">
                </div>
                <p class="tip">Repeat you password again</p>
            </div>
            <div class="control-group required">
                <label for="country">Country</label>
                <div class="control">
                    <input name="text" id="country" type="text" placeholder="Please add country">
                </div>
            </div>
            <div class="control-group required">
                <label for="postcode">Post code</label>
                <div class="control">
                    <input name="text" id="postcode" type="text" placeholder="Please add your post code">
                </div>
            </div>
            <div class="control-group">
                <label for="add-text">Additional information</label>
                <div class="control">
                    <textarea id="add-text"></textarea>
                </div>
                <p class="tip">Text length: <span id="char-count">0</span></p>
            </div>
            <button class="ink-button push-right" type="submit">Submit</button>
          </fieldset>
    </form>
		</main>

		<script src="../js/lib/jquery-1.11.1.min.js"></script>
		<script src="../js/lib/bootstrap.min.js"></script>
		<script src="../js/lib/jquery.mCustomScrollbar.concat.min.js"></script>
		<script src="../js/lib/lodasher.js"></script>
		<script src="../js/lib/jquery.cookie.min.js"></script>
		<script src="../js/template-helpers/lodash.heplers.js"></script>
		<script src="../build/js/all.min.js"></script>
	</body>

	</html>