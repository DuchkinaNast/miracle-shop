<?php
session_start();
include('config.php');
include('functions.php');
include('auth_cookie.php');

if (isset($_POST["submitdata"])) {
    $_SESSION["order_delivery"] = $_POST["order_delivery"];
    $_SESSION["order_payment"] = $_POST["order_payment"];
}

?>

<!DOCTYPE html>

<head lang="en">
    <meta charset="UTF-8">
    <title>Cart</title>
    <link rel="stylesheet" href="css/media/media4.css" type="text/css">
    <link rel="stylesheet" href="css/media/media_order2.css" type="text/css">
    <link rel="stylesheet" href="css/style.css" type="text/css">
    <link rel="stylesheet" href="css/style_cart.css" type="text/css">
    <script type="text/javascript" src="js/jq.js"></script>
</head>

<body>

<div id="pagewrap">
    <?php include("include/block-header.htm"); ?>
    <?php
    $action = $_GET["action"];
    switch ($action) {
        case 'onclick':
            echo '<div id="block-step"><div id="name-step">
            <ul>
            <li><a class="active" href="order.php?action=onclick">1.Корзина товаров</a></li>
            <li><span>&rarr;</span></li>
            <li><a href="order.php?action=confirm">2.Детали заказа</a></li>        
            </div>
            <p>шаг 1 из 2</p>
            </div>';
            echo '<div id="content">
            <div class="constructor" id="forms">
            <h2 id="ord"></h2>
            <form id="form1"  method="post" onload="save()">
            <div id="result" class="clearfix"></div> 
            <a href="order.php?action=confirm"><input type="button" id="buts" value="Оформить заказ"></a><br/>
            </form>
            </div>
            </div>';
            break;
        case 'confirm':
            echo '<div id="block-step"><div id="name-step">
            <ul>
            <li><a href="order.php?action=onclick">1.Корзина товаров</a></li>
            <li><span>&rarr;</span></li>
            <li><a  class="active" href="order.php?action=confirm"">2.Детали заказа</a></li>
            </ul>
            </div>
            <p>шаг 2 из 2</p>
            </div>';
            if ($_SESSION['order_delivery'] == "1") $chck1 = "checked";
            if ($_SESSION['order_delivery'] == "2") $chck2 = "checked";
            if ($_SESSION['order_delivery'] == "3") $chck3 = "checked";
            if ($_SESSION['order_payment'] == "1") $chck4 = "checked";
            if ($_SESSION['order_payment'] == "2") $chck5 = "checked";

            echo '<div id="content"><div class="constructor" id="forms">
<form method="post" action="include/to_order.php">
						<div id="left_block" class="left_block">
						<h3 class="title-h3" >Способы доставки:</h3>
						
						<ul id="info-radio">
							<li>
								<input type="radio" name="order_delivery" class="order_delivery" id="order_delivery1" value="1" ' . $chck1 . '  />
								<label class="label_delivery" for="order_delivery1">По почте</label>
							</li>
							<li>
								<input type="radio" name="order_delivery" class="order_delivery" id="order_delivery2" value="2" ' . $chck2 . ' />
								<label class="label_delivery" for="order_delivery2">Курьером</label>
							</li>
							<li>
								<input type="radio" name="order_delivery" class="order_delivery" id="order_delivery3" value="3" ' . $chck3 . ' />
								<label class="label_delivery" for="order_delivery3">Самовывоз</label>
							</li>
						</ul>
						<h3 class="title-h3" >Способы оплаты:</h3>
						<ul id="info-radio2">
							<li>
								<input type="radio" name="order_payment" class="order_payment" id="order_payment1" value="1" ' . $chck4 . '  />
								<label class="label_payment" for="order_payment1">наличный</label>
							</li>
							<li>
								<input type="radio" name="order_payment" class="order_payment" id="order_payment2" value="2" ' . $chck5 . ' />
								<label class="label_payment" for="order_payment2">безналичный</label>
							</li>
						</ul>
						</div>
            <input type="submit" name="submitdata" id="confirm-button" value="Оформить заказ" /> <br/>
            </form>
            </div>
						
            </div>';

            break;
        default:
            echo '<div id="block-step"><div id="name-step">
            <ul>
            <li><a class="active" href="order.php?action=onclick">1.Корзина товаров</a></li>
            <li><span>&rarr;</span></li>
            <li><a href="order.php?action=confirm">2.Детали заказа</a></li>
            </ul>
            </div>
            <p>шаг 1 из 2</p>
            </div>';
            echo '<div id="content">
            <div class="constructor" id="forms">
            <h2 id="ord"></h2>
            <form id="form1"  method="post" onload="save()">
            <div id="result" class="clearfix"></div> 
            </form></div>
            </div>';
            break;
    }
    ?>
    <?php include("include/block-sidebar.htm"); ?>
</div>
<?php include("include/block-left-menu.htm"); ?>

<script type="text/javascript" src="js/jq.js"></script>
<script type="text/javascript" src="js/order.js"></script>
<script type="text/javascript" src="js/count_tovara.js"></script>
</body>

</html>