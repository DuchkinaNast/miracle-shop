<?php

$connect = mysqli_connect('localhost', 'root', 'root', 'shop');//Создаем подключение к СУБД

if ($connect) {//Если подключение успешно, то
    $connected = mysqli_select_db($connect, 'shop') or die(mysqli_error($connect));//Выбарем БД с которой будем работать

    if (!$connected) {//если не получилось соединится С БД
        echo "Нет соединения с БД";//Выводим ошибку
        exit;//закрываем работу интерпритатора
    }

    mysqli_query($connect, 'SET NAMES \'utf8\'');//указываем БД с какой кодировкой будем работать
}
