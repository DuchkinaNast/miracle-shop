<?php

include('include/functions.php');
session_start();
include('include/auth_cookie.php');
//unset($_SESSION['auth']);
setcookie('rememberme', '', 0, "/");
?>

<!doctype html>
<html lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Дивосад</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="css/lib/bootstrap.min.css"/>
    <!--		<link rel="stylesheet" href="css/lib/uui-all.css" />-->
    <link rel="stylesheet" href="css/lib/jquery.mCustomScrollbar.css"/>
    <link rel="stylesheet" href="css/lib/jquery-ui.min.css">
    <link rel="stylesheet" href="css/lib/jquery-ui.structure.min.css">
    <link rel="stylesheet" href="css/lib/jquery-ui.theme.min.css">
    <link rel="stylesheet" href="fonts/font-awesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="build/css/all.min.css">
    <link rel="stylesheet" href="css/lib/normalize.css">
</head>
<body class="body">
<header class="header">
    <nav class="navbar navbar-inverse">
        <div class="container">
            <div class="navbar-header">
                <button class="navbar-toggle" data-toggle="collapse" data-target="#nav">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"><img src="images/logo2.png" alt="HoneyShop"/></a>
            </div>
        </div>
        <div class="collapse navbar-collapse navbar-mystyle" id="nav">
            <div class="container">
                <ul class="nav navbar-nav navbar-right text-uppercase">
                    <li>
                        <a href="#home" id="home" class="nav-item none-underline"><i
                                    class="glyphicon glyphicon-home nav-icon"></i>Главная</a>
                    </li>
                    <li>
                        <a href="#cart" id="cart" class="nav-item none-underline"><i
                                    class="glyphicon fa fa-shopping-cart nav-icon"></i>Корзина</a>
                    </li>
<!--                    <li>-->
<!--                        <a href="#providers" id="providers" class="nav-item none-underline"><i-->
<!--                                    class="glyphicon glyphicon-user nav-icon"></i>providers</a>-->
<!--                    </li>-->
                    <li>
                        <a href="#" id="" class="nav-item none-underline dropdown-toggle" data-toggle="dropdown"
                           role="button" aria-haspopup="true" aria-expanded="false"><i
                                    class="glyphicon fa fa-plus-square nav-icon"></i>Регистрация<span
                                    class="caret"></span></a>
                        <ul class="dropdown-menu additional-menu">
                            <li><a href="" id="provider" id="providers"><i
                                            class="glyphicon glyphicon-briefcase nav-icon"></i>Как поставщик</a></li>
                            <li role="separator" class="divider nav-separator"></li>
                            <li><a href="" id="user"><i class="glyphicon glyphicon-user nav-icon"></i>Как пользователь</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="" id="login" class="nav-item none-underline"><i
                                    class="glyphicon glyphicon-log-in nav-icon"></i>Войти</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>

<main class="main container">
    <aside class="sidebar clearfix col-xs-2">
        <div class="sort-header">
            Сортировка товаров
        </div>
        <form class="sidebar-item sidebar-item--first" action="#">
            <label class="label-block" for="sort">Способ сортировки:</label>
            <select name="sort" id="sort">
                <option value="default" selected="selected">по дате добавления</option>
                <option value="price-asc">по возрастанию цены</option>
                <option value="price-desc">по убыванию цены</option>
                <option value="title-asc">От А до Я</option>
                <option value="prod-reiting-desc">по рейтингу</option>
            </select>
        </form>
        <div class="sort-header">Фильтрация товара</div>

        <form class="sidebar-item clearfix" action="#">
            <label class="label-block" for="type">Тип растения:</label>
            <select name="type" id="type">
                <option>Роза плетистая</option>
                <option>Роза почвопокровная</option>
                <option>Роза английская</option>
                <option>Хвойные культуры</option>
                <option>Крупномеры декорация</option>
            </select>

            <label class="label-block" for="amount">Цена:</label>
            <input type="text" id="amount" readonly style="border:0; color:#f6931f; font-weight:bold;">

            <div id="slider-range"></div>

            <label class="label-block" for="season">Время цветения:</label>
            <select name="season" id="season">
                <option>май</option>
                <option selected="selected">июнь</option>
                <option>июль</option>
            </select>
            <button class="btn custom-btn col-xs-12 find-btn">ПОИСК</button>
        </form>
    </aside>

    <div class="content clearfix col-xs-10">
        <!-- CONTENT WILL BE HERE-->
    </div>
</main>

<script src="js/lib/jquery-1.11.1.min.js"></script>
<script src="js/lib/jquery-ui.min.js"></script>
<script src="js/lib/bootstrap.min.js"></script>
<script src="js/lib/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="js/lib/lodasher.js"></script>
<script src="js/lib/backbone.js"></script>
<script src="js/lib/underscore-min.js"></script>
<script src="js/lib/jquery.cookie.min.js"></script>
<script src="js/template-helpers/lodash.heplers.js"></script>
<script src="build/js/all.min.js"></script>

</body>
</html>
