var passValidationError = 'Пароль должен содержать не менее 7 символов!';
var passRepearValidationError = 'Поле "Повторите пароль" должно совпадать с полем пароля';
var emailValidationError = 'Невалидное значение для поля email';
var requiredValidationError = 'Заполните все обязательные поля';
var errorText = '<b>Danger!</b> Введенные данные некорректны, пожалуйста, исправьте выделенные поля! <br>';

var home,
	find = document.querySelector('.find-btn'),
	cart,
	providers,
	provider,
	user,
	loadash,
	json,
	login,
	count = 0,
	summ = 0,
	minPriceProp,
	maxPriceProp,
	typeProp,
	seasonProp,
	product = [];

$("#sort").on("selectmenuchange", function (event, ui) {
	var sorby = $('#sort').selectmenu().find('option:selected').val();

	loadData('include/res.php?sortby=' + sorby, function (res) {
		json = JSON.parse(res);
		var path = "templates/lodash.html";
		templateCard(path, json);
	});
});

find.addEventListener("click", function (evt) {
	evt.preventDefault();
	typeProp = $('#type').selectmenu().find('option:selected').val();
	seasonProp = $('#season').selectmenu().find('option:selected').val();
	select(json, typeProp, minPriceProp, maxPriceProp, seasonProp);
});

var getSumm = function (price, count) {
	return price * count;
};

function templateCart(path) {
	loadData(path, function (res) {
		var summ = 0;
		var container = document.querySelector('.content');
		container.innerHTML = '';
		var prod = JSON.parse(localStorage.getItem('cart'));
		if (prod != "") {
			for (var key in json) {
				for (var i in prod) {
					if (json[key].id == prod[i].productId) {
						tmpl = _.template(res);
						container.innerHTML += tmpl({
							product: json[key],
							count: prod[i].productCount
						});
						summ += prod[i].productCount * json[key].price;
					}
				}
			}
			container.innerHTML += '<div class="col-xs-12"><h3 class="summ-msg col-xs-12 col-sm-9">ИТОГО: ' + summ + 'грн</h3><button class="btn pull-right custom-btn col-xs-12 col-sm-3 btn-order">ОФОРМИТЬ</button></div>';
		} else {
			container.innerHTML = '<h3 class="empty-msg">Ваша корзина пуста!</h3>';
		}
	});
}

function templateCard(path, data) {
	loadData(path, function (res) {
		tmpl = _.template(res);
		var container = document.querySelector('.content');
		container.innerHTML = tmpl({
			json: data
		});
	});
}

function loadData(url, res) {
	var xhr = new XMLHttpRequest();
	xhr.onload = function () {
		if (xhr.readyState == 4 && xhr.status == 200) {
			res(xhr.responseText);
		} else {
			alert("Error request: " + xhr.status + " " + xhr.statusText);
		}
	};
	xhr.open("get", url, true);
	xhr.send();
}

loadData("include/res.php", function (res) {
	var path = "templates/lodash.html";
	json = JSON.parse(res);
	home = document.getElementById("home");
	cart = document.getElementById("cart");
	providers = document.getElementById("providers");
	user = document.getElementById("user");
	provider = document.getElementById("provider");
	login = document.getElementById("login");


	home.addEventListener("click", function (evt) {
		evt.preventDefault();
		$('#home').parent().addClass('active').siblings().removeClass('active');
		var path = "templates/lodash.html";
		templateCard(path, json);
	});

	cart.addEventListener("click", function (evt) {
		evt.preventDefault();
		select(json, "200", "июнь");
		$('#cart').parent().addClass('active').siblings().removeClass('active');
		var path = "templates/cart.html";
		templateCart(path);
		var container = document.querySelector('.content');
		container.addEventListener("click", function (event) {
			var target = event.target;
			if (target.className == "btn pull-right custom-btn col-xs-12 col-sm-3 btn-order") {
				var path = "templates/order.html";
				templateCard(path, json);
			}
		});

	});

	// providers.addEventListener("click", function (evt) {
	// 	evt.preventDefault();
	// 	$('#providers').parent().addClass('active').siblings().removeClass('active');
	// 	var path = "templates/lodash.html";
	// 	templateCard(path, json);
	// });



	user.addEventListener("click", function (evt) {
		evt.preventDefault();
		var path = "templates/reg_user.html";
		templateCard(path);

		var container = document.querySelector('.content');
		container.addEventListener("click", function (event) {
			var target = event.target;


			// var phone_pttrn = /^\d{10}$/,
				var phone_pttrn = new RegExp('/^\([0-9]{3}\)[0-9]{3}-[0-9]{2}\-[0-9]{2}$/'),
				containNumber = new RegExp('(/[0-9]+/)'),
				containLetter = new RegExp('(/[a-z]+/)');
			var pass_indicator = function () {
				if (reg_pass.value.length > 0 && reg_pass.value.length <= 4) {
					progress.className = "psw-strength weak";
				} else if (reg_pass.value.length > 4 && reg_pass.value.length < 7) {
					progress.className = "psw-strength medium";
				} else if (reg_pass.value.length >= 7) {
					progress.className = "psw-strength strong";
				} else if (reg_pass.value.length == 0) {
					progress.className = "psw-strength";
				}
			};
			var pass_change = function () {
				if ((event.ctrlKey) && (event.keyCode == 69)) {
					reg_pass.type = "text";
					reg_pass_r.type = "text";
				}
				if ((event.ctrlKey) && (event.keyCode == 82)) {
					reg_pass.type = "password";
					reg_pass_r.type = "password";
				}
			};

			var form = document.querySelector('#form_reg');
			var form_block = document.querySelector('#block_form_reg');
			var progress = document.querySelector(".psw-strength");
			var reg_login = document.getElementById("reg_login");
			var reg_pass = document.getElementById("reg_pass");
			var reg_pass_r = document.getElementById("reg_pass_r");
			var reg_surname = document.getElementById("reg_surname");
			var reg_name = document.getElementById("reg_name");
			var reg_patronymic = document.getElementById("reg_patronymic");
			var reg_phone = document.getElementById("reg_phone");
			var reg_address = document.getElementById("reg_address");
			var reg_email = document.getElementById("reg_email");
			var error = document.getElementById("error2");
			error.className = "alert alert-danger hidden";
			var success = document.getElementById("error3");
			error.className = "alert alert-success hidden";

			function checkEmail() {
				return /^([A-Za-z0-9_\-.])+@([A-Za-z0-9_\-.])+\.([A-Za-z]{2,4})$/.test(reg_email.value);
			}

			function checkPhone() {
				return (phone_pttrn.test(reg_phone.value));
			}

			function checkPass() {
				return (reg_pass.value.length >= 7);
			}
			reg_pass.addEventListener("keydown", pass_change, false);
			reg_pass_r.addEventListener("keydown", pass_change, false);
			reg_pass.addEventListener("input", pass_indicator, false);

			if (target.className == "btn btn-default custom-btn reg-btn col-xs-12") {
				var validation = function (e) {
					flag = 0;
					errorMessages = '';
					for (var k = 0; k < form.elements.length - 1; k++) {
						if (form.elements[k].value == '') {
							form.elements[k].className = 'form-control error';
							flag++;

							if (flag === 1) {
                                errorMessages += requiredValidationError;
							}
						} else {
							form.elements[k].className = 'form-control valid';

							if (form.elements[k] === reg_email) {
                                if (checkEmail()) {
                                    reg_email.className = 'form-control valid';
                                } else {
                                    reg_email.className = 'form-control error';
                                    flag++;
                                    errorMessages += emailValidationError + '<br>';
                                }
							}

							if (form.elements[k] === reg_pass) {
                                if (checkPass()) {
                                    reg_pass.className = 'form-control valid';
                                } else {
                                    reg_pass.className = 'form-control error';
                                    flag++;
                                    errorMessages += passValidationError + '<br>';
                                }
							}

							if (form.elements[k] === reg_pass_r) {
                                if (reg_pass_r.value === reg_pass.value) {
                                    reg_pass_r.className = 'form-control valid';
                                } else {
                                    reg_pass_r.className = 'form-control error';
                                    flag++;
                                    errorMessages += passRepearValidationError + '<br>';
                                }
							}
							// if (checkPhone()) {
							// 	reg_phone.className = 'form-control valid';
							// } else {
							// 	reg_phone.className = 'form-control error';
							// 	flag++;
							// }
						}
					}
					if (flag > 0) {
						console.log(errorMessages);
						error2.className = "alert alert-danger";
                        error2.innerHTML = '';
                        error2.innerHTML = errorText + errorMessages;
					} else {
						$.ajax({
							type: "POST",
							url: "include/hendler-reg.php",
							data: "login=" + reg_login.value + "&pass=" + reg_pass.value + "&surname=" + reg_surname.value + "&name=" + reg_name.value + "&patronymic=" + reg_patronymic.value + "&phone=" + reg_phone.value + "&address=" + reg_address.value + "&email=" + reg_email.value,
							dataType: "html",
							cache: false,
							success: function (data) {
								console.log(data);
								if (data === 'true') {
									//									var path = "templates/lodash.html";
									//									templateCard(path, json);
									error3.className = "alert alert-success";
									form_block.className = "block_form_reg hidden";
								} else {
									error2.className = "alert alert-danger";
								}
							},
							error: function (data) {
                                error2.className = "alert alert-danger";
                                error2.innerHTML = '';
                                error2.innerHTML = errorText + data.responseText;
                            }
						});
					}
				};
				validation();

			}
		});
	});

	login.addEventListener("click", function (evt) {
		evt.preventDefault();
		var path = "templates/auth_user.html";
		templateCard(path);

		var container = document.querySelector('.content');
		container.addEventListener("click", function (event) {
			var target = event.target;
			if (target.className == "btn btn-default custom-btn col-xs-12 col-sm-6 auth_btn") {
				var auth_login = document.getElementById("login-inp").value;
				var auth_password = document.getElementById("pass-inp").value;
				var error = document.getElementById("error");
				error.className = "alert alert-danger hidden";

				if (auth_login.length == 0 || auth_login.length > 25) {
					send_login = 'no';
					error.className = "alert alert-danger";
				} else {
					send_login = 'yes';
				}
				if (auth_password == "" || auth_password.length > 15) {
					send_password = 'no';
					error.className = "alert alert-danger";
				} else {
					send_password = 'yes';
				}
				if ($("#rememberme").prop('checked')) {
					auth_rememberme = 'yes';

				} else {
					auth_rememberme = 'no';
				}

				if (send_login === 'yes' && send_password === 'yes') {
					$.ajax({
						type: "POST",
						url: "include/auth.php",
						data: "login=" + auth_login + "&pass=" + auth_password + "&rememberme=" + auth_rememberme,
						dataType: "html",
						cache: false,
						success: function (data) {
							if (data === 'yes_auth') {
								var path = "templates/lodash.html";
								templateCard(path, json);
								login.className = "nav-item none-underline hidden";
							} else {
								error.className = "alert alert-danger";
							}
						}
					});
				}
			}
		});
	});

	//	provider.addEventListener("click", function (evt) {
	//		evt.preventDefault();
	//		var path = "templates/reg_user.html";
	//		templateCard(path);
	//	});

	templateCard(path, json);
});
