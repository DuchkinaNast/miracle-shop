        count = 0;
        summ = 0;

        function getCartData() {
        	return JSON.parse(localStorage.getItem('cart'));
        }

        function Add() {
        	var cartData = getCartData();
        	// если что-то в корзине уже есть, начинаем формировать данные для вывода
        	if (localStorage.getItem('cart').length > 2) {
        		div1 = document.createElement('div');
        		div1.class = "item";
        		for (var items in cartData) {
        			div1.id = items;
        			div1.name = cartData[items][0];
        			div1.innerHTML += '<div class="block-list-cart"><div class="title"><p>' + cartData[items][0] + '</p></div>' +
        				'<div class="img-cart"><img width="100px" height="90px" src="images/' + cartData[items][3] + '"/></div>' +
        				'<div class="title-cart">' +
        				'<p class="cart-mini-features" align="justify">' + cartData[items][2] + '</p></div>' +
        				'<div class="count-cart">' +
        				'<button class="count-minus" id="' + items + '" onclick="minus(this.id)">-</button>' +
        				'<p>' + cartData[items][4] + '</p>' +
        				'<button class="count-plus" id="' + items + '" onclick="plus(this.id)">+</button>' +
        				'</div>' +
        				'<div class="price-product"><h5><span id="count" class="span-count">' + cartData[items][4] + '</span>x<span>' + cartData[items][1] + "грн." + '</span></h5><p id="summ">' + cartData[items][1] * cartData[items][4] + "грн." + '</p></div>' +
        				'<div class="delete-cart"><img src="images/delete.png" id="' + items + '" width="30px" height="30px" onclick="deleteField(this.id, this)"/</div> ' +
        				'</div><br/><br/>';
        			document.getElementById("result").appendChild(div1);
        		}

        		div2 = document.createElement('div');
        		div2.innerHTML += '<div class="summ"><p id="itog">Итого:</p><p id="summ">' + localStorage.getItem('summ') + "грн." + '</p></div>';
        		document.getElementById("result").appendChild(div2);

        	} else {
        		var h = document.getElementById("ord");
        		h.innerHTML = "Ваша корзина пуста!";
        		document.getElementById("form1").style.display = "none";
        		document.getElementById("block-step").style.display = "none";
        	}
        }
        window.onload = function () {
        	Add();
        };

        function deleteField(items, but) {
        	var cartData = getCartData(); // получаем данные корзины или создаём новый объект, если данных еще нет // родительский элемент кнопки &quot;Добавить в корзину&quot;
        	itemId = items, // ID товара
        		itemTitle = cartData[items][0], // название товара
        		itemPrice = cartData[items][1],
        		itemInfo = cartData[items][2],
        		itemPicture = cartData[items][4];
        	cartData[itemId] = [itemTitle, itemPrice, itemInfo, itemPicture, 1];
        	delete cartData[items];
        	setCartData(cartData);
        	Add();

        }


        function setCartData(obj) {
        	localStorage.setItem('cart', JSON.stringify(obj));
        	document.getElementById("result").removeChild(div1);
        	document.getElementById("result").removeChild(div2);
        	summ = 0;
        	count = 0;
        	Add_count();
        }

        function plus(items) {
        	var cartData = getCartData(), // получаем данные корзины или создаём новый объект, если данных еще нет // родительский элемент кнопки &quot;Добавить в корзину&quot;
        		itemId = items, // ID товара
        		itemTitle = cartData[items][0], // название товара
        		itemPrice = cartData[items][1],
        		itemInfo = cartData[items][2],
        		itemPicture = cartData[items][4]; // стоимость товара
        	if (cartData.hasOwnProperty(itemId)) { // если такой товар уже в корзине, то добавляем +1 к его количеству
        		cartData[itemId][4] += 1;
        	} else { // если товара в корзине еще нет, то добавляем в объект
        		cartData[itemId] = [itemTitle, itemPrice, itemInfo, itemPicture, 1];
        	}
        	// Обновляем данные в LocalStorage
        	setCartData(cartData);

        	Add();

        }

        function minus(items) {
        	var cartData = getCartData(), // получаем данные корзины или создаём новый объект, если данных еще нет // родительский элемент кнопки &quot;Добавить в корзину&quot;
        		itemId = items, // ID товара
        		itemTitle = cartData[items][0], // название товара
        		itemPrice = cartData[items][1],
        		itemInfo = cartData[items][2],
        		itemPicture = cartData[items][4]; // стоимость товара
        	if (cartData.hasOwnProperty(itemId)) { // если такой товар уже в корзине, то добавляем +1 к его количеству
        		cartData[itemId][4] -= 1;
        	} else { // если товара в корзине еще нет, то добавляем в объект
        		cartData[itemId] = [itemTitle, itemPrice, itemInfo, itemPicture, 1];
        	}
        	// Обновляем данные в LocalStorage
        	setCartData(cartData);
        	Add();
        }
//        $(document).ready(function () {
//        	$("#buts").click(function () {
//        		if (send() == true) {
//        			var name = $("#name").val();
//        			var adr = $("#adre").val();
//        			var tel = $("#tel").val();
//        			var mail = $("#mail").val();
//        			var order = localStorage.cart;
//        			datainfo = "data=" + name + "&data1=" + adr + "&data2=" + tel + "&data3=" + mail + "&data4=" + order;
//
//        			jQuery.ajax({
//        				type: "POST", // HTTP метод  POST или GET
//        				url: "response.php", //url-адрес, по которому будет отправлен запрос
//        				dataType: "text", // Тип данных,  которые пришлет сервер в ответ на запрос ,например, HTML, json
//        				data: datainfo,
//        				success: function (response) {},
//        				error: function (xhr, ajaxOptions, thrownError) {
//        					alert(thrownError); //выводим ошибку
//        				}
//        			});
//        			document.getElementById("form1").style.display = "none";
//        			document.getElementById("forms").style.color = "green";
//        			document.getElementById("forms").style.fontSize = "22px";
//        			document.getElementById("forms").innerHTML += "<p style='text-align:center;'>Ваш заказ успешно принят!</p>";
//        			document.getElementById("forms").innerHTML += "<p style='text-align:center;'>С вами свяжется наш менеджер!</p>";
//        			document.getElementById("forms").innerHTML += "<p style='text-align:center;'><a href='index.php'><img src='images/OK.png'/></a></p>";
//        			Add();
//        		} else {
//        			i = 0;
//        			send();
//        		}
//        	});
//        });
//        var i = 0;
//
//        function checkname() {
//        	var val = document.getElementById("name").value;
//        	if (val == "") {
//        		document.getElementById("err1").innerHTML = "Поле имя должно быть заполнено";
//        		document.getElementById("err1").style.color = "red";
//        		document.getElementById("err1").style.fontSize = "14px";
//
//        	} else {
//        		document.getElementById("err1").style.display = "none";
//        		i++;
//        	}
//        }
//
//        function checktel() {
//        	var val = document.getElementById("tel").value;
//        	if (val == "") {
//        		document.getElementById("err2").innerHTML = "Поле телефон должно быть заполнено";
//        		document.getElementById("err2").style.color = "red";
//        		document.getElementById("err2").style.fontSize = "14px";
//        	} else {
//        		document.getElementById("err2").style.display = "none";
//        		i++;
//        	}
//        }
//
//        function checkadr() {
//        	var val = document.getElementById("adre").value;
//        	if (val == "") {
//        		document.getElementById("err3").innerHTML = "Поле адрес должно быть заполнено";
//        		document.getElementById("err3").style.color = "red";
//        		document.getElementById("err3").style.fontSize = "14px";
//        	} else {
//        		document.getElementById("err3").style.display = "none";
//        		i++;
//        	}
//        }
//
//        function checkmail() {
//        	var val = document.getElementById("mail").value;
//        	var mail_pattern = /^(([a-zA-Z0-9]|[!#$%\*\/\?\|^\{\}`~&'\+=-_])+\.)*([a-zA-Z0-9]|[!#$%\*\/\?\|^\{\}`~&'\+=-_])+@([a-zA-Z0-9-]+\.)+[a-zA-Z0-9-]+$/; //
//        	var test = mail_pattern.test(val);
//        	if ((val == "") || (test == false)) {
//        		document.getElementById("err4").innerHTML = "Формат почтового адреса неверен.";
//        		document.getElementById("err4").style.color = "red";
//        		document.getElementById("err4").style.fontSize = "14px";
//        	} else {
//        		document.getElementById("err4").style.display = "none";
//        		i++;
//        	}
//        }
//
//        function send() {
//        	checkadr();
//        	checkname();
//        	checktel();
//        	checkmail();
//        	if (i == 4 || i == 8) {
//
//        		return true;
//
//        	} else {
//        		return false;
//
//        	}
//        }