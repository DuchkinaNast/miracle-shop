var getDaysAmount = function (value) {
	date = (new Date().getTime() / (1000 * 60 * 60 * 24));
	value = (new Date(value).getTime() / (1000 * 60 * 60 * 24));
	return (Math.round(date) - Math.round(value));
};

var toLowerCase = function (value) {
	if (value && typeof value === 'string') {
		return value.toLowerCase();
	} else {
		return '';
	}
};

var substring = function (value) {
	if (value && typeof value === 'string') {
		return (value.replace(/(\w+,\s){2}/, ""));
	} else {
		return '';
	}
};

var concat = function (val1, val2, val3) {
	return val1+" "+val2+" "+val3;
}

var rating = function (val) {
	if (val!=null){
	return parseInt(val)+'%';
	}
	else return '0%'
}