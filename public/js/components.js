$(function () {
	$("#type").selectmenu();
	$("#season").selectmenu();
	$("#sort").selectmenu();
	$("#slider-range").slider({
		range: true,
		min: 0,
		max: 300,
		values: [50, 1000],
		slide: function (event, ui) {
			$("#amount").val(ui.values[0] + "грн" + " - " + ui.values[1] + "грн");
			minPriceProp = ui.values[0];
			maxPriceProp = ui.values[1];
		}
	});
	$("#amount").val($("#slider-range").slider("values", 0) + "грн" + " - " + $("#slider-range").slider("values", 1) + "грн");
	minPriceProp = $("#slider-range").slider("values", 0);
	maxPriceProp = $("#slider-range").slider("values", 1);
});