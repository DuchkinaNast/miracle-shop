var ProductsCollection = {
	initialize: function () {
		this.cart = JSON.parse(localStorage.getItem('cart')) || [];
		return this.cart;
	},

	getCartInfo: function () {
		return this.cart;
	},

	clearCollection: function (cityObj) {
		this.cart = [];
		localStorage.setItem('cart', JSON.stringify(this.cart));
		return this.cart;
	},

	addProduct: function (productObj) {
		this.cart.push(productObj);
		localStorage.setItem('cart', JSON.stringify(this.cart));
	},
	minus: function (uniqId) {
		var index = this.getProductById(uniqId, this.cart);
		if (this.cart[index].productCount != 1) {
			this.cart[index].productCount--;
		} else {
			this.cart[index].productCount = 1;
		}
		localStorage.setItem('cart', JSON.stringify(this.cart));
		templateCart(path);
	},
	plus: function (uniqId) {
		var index = this.getProductById(uniqId, this.cart);
		this.cart[index].productCount++;
		localStorage.setItem('cart', JSON.stringify(this.cart));
	},
	removeProduct: function (uniqId) {
		var index = this.getProductById(uniqId, this.cart);
		this.cart.splice(index, 1);
		localStorage.setItem('cart', JSON.stringify(this.cart));
		templateCart(path);
	},

	getProductById: function (uniqId) {
		for (var key in this.cart) {
			if (this.cart[key].productId === uniqId) {
				return (key);
			}
		}
	}
};

function ProductModel(productObj) {
	this.productId = productObj.id || 1;
	this.productName = productObj.name;
	this.productInfo = productObj.mini_description;
	this.productPrice = productObj.price;
	this.productCount = productObj.count || 1;
	return this;
}
var path = "templates/cart.html";
var cartData = ProductsCollection.initialize();

function addToCart(e) {
	if (ProductsCollection.getProductById(e)) {
		ProductsCollection.plus(e);
	} else {
		ProductsCollection.addProduct(new ProductModel({
			id: e
		}));
	}
}

function plus(e) {
	var prodId = ProductsCollection.getProductById(e);
	if (prodId) {
		ProductsCollection.plus(e);
		var prodCount = ProductsCollection.getCartInfo()[prodId].productCount;
		templateCart(path);
	}
}

function minus(e) {
	var prodId = ProductsCollection.getProductById(e);
	if (prodId) {
		ProductsCollection.minus(e);
		var prodCount = ProductsCollection.getCartInfo()[prodId].productCount;
	}
}

function removeProd(e) {
	ProductsCollection.removeProduct(e);
}

function clearCart() {
	ProductsCollection.clearCollection();
}
