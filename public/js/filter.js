function select(arr, type, minPrice, maxPrice, season) {
	
	var filterResult = [];
	for (var k = 0; k < arr.length; k++) {
		if ((arr[k].properties.category === type) && (arr[k].price * 1 >= minPrice) && (arr[k].price * 1 <= maxPrice) && (arr[k].properties.season == season)) {
			filterResult.push(arr[k]);
		}
	}
	
	if ($("#checkPareto").is(':checked')) {
		for (var i = 0; i < filterResult.length; i++) {
			for (var j = i; j < filterResult.length; j++) {
				if (arr[i].properties.liquid > arr[j].properties.liquid) {
					filterResult.splice(j, 1);
					i--;
				} else if (arr[i].properties.liquid < arr[j].properties.liquid) {
					filterResult.splice(i, 1);
					j--;
				} else if (arr[i].properties.liquid === arr[j].properties.liquid) {
					console.log(arr[i].properties.liquid +" "+ arr[j].properties.liquid);
					if (arr[i].properties.lightness > arr[j].properties.lightness) {
						filterResult.splice(j, 1);
						i--;
					} else if (arr[i].properties.lightness < arr[j].properties.lightness) {
							filterResult.splice(i, 1);
							j--;
					}
					else if (arr[i].properties.lightness === arr[j].properties.lightness) {
							console.log(filterResult);
						i++; 
						j++;
						}
				}
			}
		}
	}
	if (filterResult.length >0) {
	templateCard("templates/lodash.html", filterResult);
	}
	else {
		document.querySelector('.content').innerHTML = '<h3 class="empty-msg">По вашему запросу ничего не найдено!</h3>';
	}
}
