var gulp = require('gulp'),
  concat = require('gulp-concat'),
  jshint = require('gulp-jshint'),
  sass = require('gulp-sass'),
  minify = require('gulp-minify-css'),
  sourcemaps = require('gulp-sourcemaps'),
  uglify = require('gulp-uglify'),
  watch = require('gulp-watch');

var paths = {
    sass: 'public/sass/*.scss',
    js: 'public/js/*.js'
  },
  build = {
    root: 'public/build/',
    css: 'public/build/css',
    js: 'public/build/js'
  };

gulp.task('default', ['lint', 'js', 'sass', 'watch']);

gulp.task('lint', function () {
  return gulp.src(paths.js)
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});

gulp.task('sass', function () {
  return gulp.src([paths.sass])
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(concat('all.min.css'))
    .pipe(minify())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(build.css));
});

gulp.task('js', function () {
  return gulp.src([paths.js])
    .pipe(sourcemaps.init())
    .pipe(concat('all.min.js'))
    .pipe(uglify())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(build.js))
});

gulp.task('watch', function () {
  watch(paths.sass, function (event, cb) {
    gulp.start('sass');
  });

  watch(paths.js, function (event, cb) {
    gulp.start('lint', 'js');
  });

});
