#!/bin/bash

DB_USER='root'
DB_PORT='3306'
DB_PASSWORD='root'
DB_NAME='shop'
DISPLAY_ERRORS="on"
DB_NAME_TEST='shop_test'

echo "Updating packages..."
echo vagrant | sudo -S apt-get update

echo "Installing mc..."
apt-get install -y mc 2> /dev/null

echo "Installing git..."
echo vagrant | sudo apt-get install git -y > /dev/null 2>&1

echo "Installing sendmail..."
echo vagrant | sudo apt-get install sendmail -y > /dev/null 2>&1

echo "Installing nginx..."
echo vagrant | sudo apt-get install -y nginx > /dev/null 2>&1

echo "Installing glp..."
echo vagrant | sudo apt-get install -y nginx > /dev/null 2>&1

echo "Setting nginx..."
echo vagrant | sudo rm -rf /usr/share/nginx/html
echo vagrant | sudo ln -s /vagrant/public /usr/share/nginx/html
echo vagrant | sudo -S cp /vagrant/nginx.config /etc/nginx/sites-available/default
echo vagrant | sudo -S service nginx restart

echo "Installing mysql 5.7..."
debconf-set-selections <<< "mysql-community-server mysql-community-server/root-pass password $DB_PASSWORD"
debconf-set-selections <<< "mysql-community-server mysql-community-server/re-root-pass password $DB_PASSWORD"
echo vagrant | sudo apt-key adv --keyserver ha.pool.sks-keyservers.net --recv-keys 5072E1F5
echo vagrant | echo "deb http://repo.mysql.com/apt/ubuntu/ trusty mysql-5.7" | sudo tee /etc/apt/sources.list.d/mysql-5.7.list
echo vagrant | sudo apt-get update
echo vagrant | sudo DEBIAN_FRONTEND=noninteractive apt-get install -qy mysql-server
echo vagrant | sudo -S sed -i "s/^bind-address.*127.0.0.1/bind-address=0.0.0.0/" /etc/mysql/my.cnf

echo "Creating DB..."
mysql -uroot -p$DB_PASSWORD -e "CREATE DATABASE IF NOT EXISTS $DB_NAME CHARACTER SET utf8 COLLATE utf8_general_ci;" >> /vagrant/vm_build.log 2>&1

echo "Creating DB for test environment"
mysql -uroot -p$DB_PASSWORD -e "CREATE DATABASE IF NOT EXISTS $DB_NAME_TEST CHARACTER SET utf8 COLLATE utf8_general_ci;" >> /vagrant/vm_build.log 2>&1

echo "Installing php 7.1..."
echo vagrant | sudo -S add-apt-repository ppa:ondrej/php
echo vagrant | sudo -S apt-get update
echo vagrant | sudo -S apt-get install php7.1 php7.1-cgi php7.1-fpm php7.1-zip  php7.1-curl php7.1-dom php7.1-mysql php7.1-gd php7.1-mbstring -y > /dev/null 2>&1

echo "Install npm and gulp"
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.1/install.sh | bash
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh" # This loads nvm
nvm install v7.7.1
nvm use v7.7.1
npm config set registry "http://registry.npmjs.org/"
npm i -g pnpm
pnpm install
./node_modules/gulp/bin/gulp.js
